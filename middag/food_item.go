package middag

import (
	"database/sql"
	"fmt"
)

type FoodItem struct {
	Id           int
	Title        string
	Brand        string
	Calories     int
	Fat          int
	Carbohydrate int
	Vitamin      int
	Price        int
}

func GetFoodItemDB(ingredient Ingredient,
	globalFoodItems *map[int]FoodItem,
	db *sql.DB,
) (err error) {

	query := "SELECT id, title, brand, price FROM fooditem where id = $1"
	rows, err := db.Query(query, ingredient.FoodItem)
	if err != nil {
		return err
	}

	defer rows.Close()

	for rows.Next() {
		var fooditem FoodItem
		if err = rows.Scan(
			&fooditem.Id,
			&fooditem.Title,
			&fooditem.Brand,
			&fooditem.Price,
		); err != nil {
			return err
		}
		(*globalFoodItems)[fooditem.Id] = fooditem
	}
	return nil

}

func (f *FoodItem)Disp(){
	fmt.Printf("%s, %s\t", f.Title, f.Brand)
}
