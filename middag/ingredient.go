package middag

import (
	"database/sql"
	"fmt"
)

type Ingredient struct {
	Amount   int
	Unit     string
	FoodItem int
}

func GetIngredients(recipe int, db *sql.DB) (ingredients []Ingredient, err error) {
	query := "SELECT amount, unit, food_item FROM ingredient WHERE recipe = $1"
	rows, err := db.Query(query, recipe)
	if err != nil {
		return []Ingredient{}, err
	}
	defer rows.Close()

	for rows.Next() {
		var ingredient Ingredient
		if err = rows.Scan(
			&ingredient.Amount,
			&ingredient.Unit,
			&ingredient.FoodItem,
		); err != nil {
			return nil, err
		}

		ingredients = append(ingredients, ingredient)
	}

	return ingredients, nil
}

func (i *Ingredient) Disp(fooditems *map[int]FoodItem) {
	fooditem := (*fooditems)[i.FoodItem]
	fooditem.Disp()
	fmt.Printf("%d\t %d %s\n",
		i.FoodItem, i.Amount, i.Unit,
	)

}
