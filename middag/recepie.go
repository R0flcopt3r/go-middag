package middag

import (
	"database/sql"
	"fmt"
)

type Recipe struct {
	Id          int
	Title       string
	Instruction string
	Portion     int
	Category    int
	Ingredients []Ingredient
}

func NewRecipe(id int, db *sql.DB) (recipe Recipe, err error) {
	row := db.QueryRow("SELECT * FROM recipe WHERE id = $1", id)
	if err = row.Scan(
		&recipe.Id, &recipe.Title,
		&recipe.Instruction, &recipe.Portion,
		&recipe.Category,
	); err != nil {
		return Recipe{}, err
	} else {
		if err = recipe.GetIngredientsDB(db); err != nil {
			return recipe, err
		}
		return recipe, nil
	}
}

func (r *Recipe) Disp(fooditems *map[int]FoodItem) {
	fmt.Printf("Title: %s"+
		"\nPortion: %d"+
		"\nInstruction: \n%s\n",
		r.Title, r.Portion, r.Instruction,
	)

	if r.Ingredients != nil {
		for _, ingredient := range r.Ingredients {
			ingredient.Disp(fooditems)
		}
	}
}

func (r *Recipe) GetIngredientsDB(db *sql.DB) (err error) {
	if r.Ingredients, err = GetIngredients(r.Id, db); err != nil {
		return err
	}
	return nil
}
