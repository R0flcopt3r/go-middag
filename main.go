package main

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
	"gitlab.com/R0flcopt3r/go-middag/middag"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "middag"
	password = "pass"
	dbname   = "middag"
)

func main() {

	foodItems := make(map[int]middag.FoodItem, 50)

	psqlInfo := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname,
	)

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	testRecipe, err := middag.NewRecipe(1, db)
	if err != nil {
		fmt.Println(err)
	} else {
		for _, ingredient := range testRecipe.Ingredients {
			middag.GetFoodItemDB(ingredient, &foodItems, db)
		}
		testRecipe.Disp(&foodItems)
	}


}
