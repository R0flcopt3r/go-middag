CREATE TABLE Recipe (
       id SERIAL PRIMARY KEY,
       title VARCHAR NOT NULL,
       instruction TEXT NOT NULL,
       portion INT NOT NULL,
       category INT
);

CREATE TABLE Ingredient (
       amount FLOAT,
       unit VARCHAR,
       food_item INT,
       recipe INT,
       PRIMARY KEY (food_item, recipe)
);

CREATE TABLE fooditem (
       id SERIAL PRIMARY KEY,
       title VARCHAR NOT NULL,
       brand VARCHAR NOT NULL,
       calories INTEGER,
       fat INTEGER,
       carbohydrate INTEGER,
       vitamin VARCHAR,
       price INTEGER
);

CREATE TABLE category (
       id SERIAL PRIMARY KEY,
       title VARCHAR NOT NULL
);

ALTER TABLE Recipe
ADD CONSTRAINT category_fk FOREIGN KEY (category) REFERENCES category(id);

ALTER TABLE Ingredient
ADD CONSTRAINT fooditem_fk FOREIGN KEY (food_item) REFERENCES fooditem(id),
ADD CONSTRAINT recipe_fk FOREIGN KEY (recipe) REFERENCES recipe(id);

INSERT INTO fooditem (
    title,
    brand,
    price
) VALUES (
    'test_title',
    'test_brand',
    30
),(
    'test_title1',
    'test_brand1',
    10
),(
    'test_title2',
    'test_brand2',
    20
);

INSERT INTO category (
       title
) VALUES
('Test'),
('Test1'),
('Test2'),
('Test3'),
('Test4');

INSERT INTO recipe (
       title,
       instruction,
       portion,
       category
) VALUES (
    'test1',
    'slik lager du test1',
    2,
    (SELECT id FROM category WHERE id=1)
),(
    'test2',
    'slik lager du test2',
    4,
    (SELECT id FROM category WHERE id=2)
);

INSERT INTO ingredient (
       food_item,
       recipe,
       amount,
       unit
) VALUES (
    (SELECT id FROM fooditem WHERE id=1),
    (SELECT id FROM recipe WHERE id=1),
    2, 'kg'
),
(
    (SELECT id FROM fooditem WHERE id=2),
    (SELECT id FROM recipe WHERE id=1),
    4, 'liter'
),
(
    (SELECT id FROM fooditem WHERE id=1),
    (SELECT id FROM recipe WHERE id=2),
    8, 'tsp'
),
(
    (SELECT id FROM fooditem WHERE id=2),
    (SELECT id FROM recipe WHERE id=2),
    2, 'klype'
);
