# go-middag

Go middag is a play on the Norwegian phrase "God middag", meaning "Have a nice
dinner". It will grab recipes from a Postgres database and create a shopping
list and output instructions on how to make it.

## Setup Environment

`make run` should run through all the required steps to run the project, including
running it.

`make build` should run through all the required steps to compile the project,
including building it.

When finished run `make clean` to bring the containers down, and removed build files.

`docker-compose.yml` contains the required Postgres database and a nice-to-have
adminer container. You can run it with either `podman-compose` or `docker-compose`.

`db_init/middag.sql` is used by the Postgres database to populate it with some
basic testdata. _This needs to be expanded._


## What needs one

- [ ] Create more testdata
- [ ] Toughen makefile

## Wanted functionality

- [ ] Save up to 7 recipes in a slice ( one for each day of the week )
  - [ ] Export shopping list based on recipes
- [ ] Prioring/ranking recipes, on the database
  - [ ] Adding categories to recipes like:
	- Weekend
	- Timeconsuming
	- Expensive
- [ ] Add maximum price per week
- [ ] Frontend
- [ ] Make it servable ( API )
