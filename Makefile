##
# go-middag
#
# @file
# @version 0.1

.PHONY: clean

build: main.go deps
	go build .

run: main.go up deps
	go run .

up: docker-compose.yml
	podman-compose -f docker-compose.yml up  --detach
	touch .make.$@

down: .make.up
	podman-compose -f docker-compose.yml down
	rm .make.up

clean: go_clean down
	rm .make.deps.file

go_clean:
	go clean

remake: clean all

.make.deps:
	go get -u github.com/lib/pq
	touch $@.file

deps: .make.deps

# end
